package Version1;

import java.util.Scanner;

public class methodes {
	// Varialbe globale
	static Scanner saisie = new Scanner(System.in);
	static boolean ok;

	public static void AffichageMenu() {
		System.out.println("\n\t>>>> Calculs Formes Géométriques <<<<\n\n");
		System.out.println("-1- CARRE\n-2- CERCLE\n-3-TRIANGLE EQUI\n");
		System.out.println("-9- Quitter\n");
		System.out.print("\tVotre choix ==> ");
	}

	public static void Carre() {
		// Déclaration des varialbles
		float perimetre_carre = 0, surface_carre = 0, cote_carre = 0;
		ok = false;
		// Saisie de l'utilisateur du coté du carré et calculs
		System.out.print("\n--> CARRE - Saisir la mesure du côté : ");

		// Test de la saisie de l'uitlisateur
		while (!ok) {
			try {
				cote_carre = saisie.nextFloat();
				ok = true;
			} catch (Exception e) {
				System.err.println("Erreur de type, saisir un nombre : ");
				String vide = saisie.nextLine();
			}
		}

		// Calculs
		perimetre_carre = cote_carre * 4;
		surface_carre = (float) Math.pow((double) cote_carre, 2);

		// Affichage des calculs réalisés
		System.out.print("\nCARRE : côté = " + cote_carre + " - périmètre = " + perimetre_carre + " - surface = "
				+ surface_carre + "\n\n");
	}

	public static void Cercle() {
		// Déclaration des varialbles
		float rayon_cercle = 0, perimetre_cercle = 0, surface_cercle = 0;
		ok = false;
		// Saisie de l'utilisateur du rayon du cercle et calculs
		System.out.print("\n\n--> CERCLE - Saisir la mesure du rayon : ");

		// Tester la saisie de l'utilisateur
		while (!ok) {
			try {
				rayon_cercle = saisie.nextFloat();
				ok = true;
			} catch (Exception e) {
				System.err.println("Erreur de type, saisir un nombre : ");
				String vide = saisie.nextLine();
			}
		}

		// Calculs
		perimetre_cercle = (float) (2 * Math.PI * rayon_cercle);
		surface_cercle = (float) ((float) Math.pow(rayon_cercle, 2) * Math.PI);

		// Affichage des calculs réalisés
		System.out.print("\nCERCLE : côté = " + rayon_cercle + " - périmètre = " + perimetre_cercle + " - surface = "
				+ surface_cercle + "\n\n");
	}

	public static void Triangle_equi() {
		// Déclaration des varialbles
		float cote_triangle_equi = 0, hauteur_triangle_equi = 0, perimetre_triangle_equi = 0, surface_triangle_equi = 0;
		ok = false;
		// Saisie de l'utilisateur du coté puis de la hauteur et calculs
		System.out.print("\n\n--> TRIANGLE EQUI - Saisir la mesure du côté : ");

		// Test de la saisie de l'utilisateur
		while (!ok) {
			try {
				cote_triangle_equi = saisie.nextFloat();
				ok = true;
			} catch (Exception e) {
				System.err.println("Erreur de type, saisir un nombre : ");
				String vide = saisie.next();
			}
		}

		System.out.print("\tsaisir la mesure de la hauteur : ");

		// Test de la saisie de l'utilisateur
		ok = false;
		while (!ok) {
			try {
				hauteur_triangle_equi = saisie.nextFloat();
				ok = true;
			} catch (Exception e) {
				System.err.println("Erreur de type, saisir un nombre : ");
				String vide = saisie.nextLine();
			}
		}

		// Calculs
		perimetre_triangle_equi = cote_triangle_equi * 3;
		surface_triangle_equi = cote_triangle_equi * hauteur_triangle_equi / 2;

		// Affichage des calculs réalisés
		System.out.print("\nTRIANGLE EQUI : côté = " + cote_triangle_equi + " - hauteur = " + hauteur_triangle_equi
				+ " - périmètre = " + perimetre_triangle_equi + " - surface = " + surface_triangle_equi + "\n\n");
	}

	public static void choix(int choix) {
		if (choix == 1) {
			methodes.Carre();
		} else if (choix == 2) {
			methodes.Cercle();
		} else if (choix == 3) {
			methodes.Triangle_equi();
		} else if (choix == 9) {
			System.out.println("\nVous avez quitter le programme...");
		} else if (choix != 9 || choix == 3 || choix == 2 || choix == 1) {
			System.err.println("\nErreur de saisir... Veuillez saisie un chiffre du menu");
		}
	}
}
