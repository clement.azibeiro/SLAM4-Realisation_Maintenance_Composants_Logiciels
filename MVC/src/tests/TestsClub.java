package tests;
import classes.Club;;

public class TestsClub {

	public static void main(String[] args) {
		Club monClub = new Club();
		Club monClub2 = new Club("MC2","C'est mon club","Clement AZIBERIO","Entraineur");
		
		System.out.println(monClub.getCode()+" "+monClub.getNom()+" "+monClub.getNom_president()+" "+monClub.getNom_entraineur());
		System.out.println(monClub2.getCode()+" "+monClub2.getNom()+" "+monClub2.getNom_president()+" "+monClub2.getNom_entraineur());
		
		monClub.setCode("CodeBis");
		monClub.setNom("NomBis");
		monClub.setNom_entraineur("Nom entraineur bis");
		monClub.setNom_president("Nom president Bis");
		
		monClub2.setCode("MC2 bis");
		monClub2.setNom("Mon club");
		monClub2.setNom_entraineur("Tuchel");
		monClub2.setNom_president("Toujours moi");
		
		System.out.println(monClub.toString());
		System.out.println(monClub2.toString());

	}

}
