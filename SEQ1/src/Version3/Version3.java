package Version3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JFormattedTextField;

public class Version3 extends JFrame {

	private JPanel contentPane;
	private JFormattedTextField textSaisieCtCarre;
	private JTextField textSaisieCtCercle;
	private JFormattedTextField textSaisieCtTriangle;
	private JFormattedTextField textSaisieHtTriangle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Version3 frame = new Version3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Version3() {
		setTitle("Calculator");
		List<Forme> liste_forme = new ArrayList<>();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 505, 702);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblErrSaisie = new JLabel("Erreur : Saisir un décimal (avec un point en tant que virgule)");
		lblErrSaisie.setForeground(Color.RED);
		lblErrSaisie.setBackground(Color.WHITE);
		lblErrSaisie.setBounds(12, 351, 481, 15);
		contentPane.add(lblErrSaisie);
		lblErrSaisie.setVisible(false);
				
		JLabel lblImgcercle = new JLabel("");
		lblImgcercle.setIcon(new ImageIcon("/home/etudiant/c.azibeiro/SEQ1/src/Version3/cercle.png"));
		lblImgcercle.setBounds(32, 96, 100, 100);
		contentPane.add(lblImgcercle);
				
		JLabel imgCarre = new JLabel(" ");
		imgCarre.setIcon(new ImageIcon("/home/etudiant/c.azibeiro/SEQ1/src/Version3/carre.png"));
		imgCarre.setBounds(32, -4, 100, 100);
		contentPane.add(imgCarre);
		
		JLabel lblPrimtreCercle = new JLabel("Périmètre = ");
		lblPrimtreCercle.setBounds(200, 141, 202, 15);
		contentPane.add(lblPrimtreCercle);
		
		JLabel lblCtCercle = new JLabel("Côté = ");
		lblCtCercle.setBounds(200, 114, 70, 15);
		contentPane.add(lblCtCercle);
		
		textSaisieCtCercle = new JFormattedTextField();
		textSaisieCtCercle.setBounds(288, 112, 114, 19);
		contentPane.add(textSaisieCtCercle);
		textSaisieCtCercle.setColumns(10);
		
		JLabel lblSurfaceCercle = new JLabel("Surface = ");
		lblSurfaceCercle.setBounds(200, 168, 202, 15);
		contentPane.add(lblSurfaceCercle);
	

		JTextArea textCalculs = new JTextArea();
		textCalculs.setEditable(false);
		textCalculs.setBounds(0, -4, 426, 215);
		contentPane.add(textCalculs);

		JScrollPane jsp = new JScrollPane(textCalculs,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jsp.setBounds(12, 438, 481, 215);
		getContentPane().add(jsp);
		
		JLabel lblCtTriangle = new JLabel("Côté = ");
		lblCtTriangle.setBounds(200, 226, 70, 15);
		contentPane.add(lblCtTriangle);

		JLabel lblHauteurTriangle = new JLabel("Hauteur = ");
		lblHauteurTriangle.setBounds(200, 253, 85, 15);
		contentPane.add(lblHauteurTriangle);

		JLabel lblPrimtreTriangle = new JLabel("Périmètre = ");
		lblPrimtreTriangle.setBounds(200, 280, 211, 15);
		contentPane.add(lblPrimtreTriangle);

		JLabel lblSurfaceTriangle = new JLabel("Surface = ");
		lblSurfaceTriangle.setBounds(200, 307, 202, 15);
		contentPane.add(lblSurfaceTriangle);

		JLabel lblCalculsRaliss = new JLabel("Calculs réalisés :");
		lblCalculsRaliss.setBounds(12, 411, 134, 15);
		contentPane.add(lblCalculsRaliss);

		JLabel lblCtCarre = new JLabel("Côté =");
		lblCtCarre.setBounds(200, 12, 70, 15);
		contentPane.add(lblCtCarre);

		textSaisieCtCarre = new JFormattedTextField();
		textSaisieCtCarre.setBounds(288, 10, 114, 19);
		contentPane.add(textSaisieCtCarre);
		textSaisieCtCarre.setColumns(10);

		JLabel lblPrimtreCarre = new JLabel("Périmètre = ");
		lblPrimtreCarre.setBounds(200, 39, 199, 15);
		contentPane.add(lblPrimtreCarre);

		JLabel lblSurfaceCarre = new JLabel("Surface = ");
		lblSurfaceCarre.setBounds(200, 64, 204, 15);
		contentPane.add(lblSurfaceCarre);

		JButton btnReinit = new JButton("Réinitialiser");
		btnReinit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Réinitialisation de l'affichage du périmètre,suface du carré
				lblPrimtreCarre.setText("Périmètre = ");
				lblSurfaceCarre.setText("Surface = ");

				// Réinitialisation de l'affichage du périmètre,suface du cercle
				lblPrimtreCercle.setText("Périmètre = ");
				lblSurfaceCercle.setText("Surface = ");

				// Réinitialisation de l'affichage du périmètre,suface du triangle
				lblPrimtreTriangle.setText("Périmètre = ");
				lblSurfaceTriangle.setText("Surface = ");

				textSaisieCtCarre.setText("");
				textSaisieCtCercle.setText("");
				textSaisieCtTriangle.setText("");
				textSaisieHtTriangle.setText("");
			}
		});
		btnReinit.setBounds(166, 378, 144, 25);
		contentPane.add(btnReinit);
		btnReinit.setVisible(false);

		JButton btnCalc = new JButton("Calculer");
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Réinitialisation de l'affichage du périmètre,suface du carré
				lblPrimtreCarre.setText("Périmètre = ");
				lblSurfaceCarre.setText("Surface = ");

				// Réinitialisation de l'affichage du périmètre,suface du cercle
				lblPrimtreCercle.setText("Périmètre = ");
				lblSurfaceCercle.setText("Surface = ");

				// Réinitialisation de l'affichage du périmètre,suface du triangle
				lblPrimtreTriangle.setText("Périmètre = ");
				lblSurfaceTriangle.setText("Surface = ");

				try {
					
					lblErrSaisie.setVisible(false);
					
				if (textSaisieCtCarre.getText().length() != 0) {
					btnReinit.setVisible(true);

					Carre monCarre = new Carre(Float.parseFloat(textSaisieCtCarre.getText()));
					lblPrimtreCarre.setText(lblPrimtreCarre.getText() + Float.toString(monCarre.perimetre()));
					lblSurfaceCarre.setText(lblSurfaceCarre.getText() + Float.toString(monCarre.surface()));

					liste_forme.add(monCarre);
				}

				if (textSaisieCtCercle.getText().length() != 0) {
					btnReinit.setVisible(true);

					Cercle monCercle = new Cercle(Float.parseFloat(textSaisieCtCercle.getText()));
					lblPrimtreCercle.setText(lblPrimtreCercle.getText() + Float.toString(monCercle.perimetre()));
					lblSurfaceCercle.setText(lblSurfaceCercle.getText() + Float.toString(monCercle.surface()));

					liste_forme.add(monCercle);
				}

				if (textSaisieCtTriangle.getText().length() != 0 && textSaisieHtTriangle.getText().length() != 0) {
					btnReinit.setVisible(true);

					Triangle_equi monTriangle = new Triangle_equi(Float.parseFloat(textSaisieCtTriangle.getText()),Float.parseFloat(textSaisieHtTriangle.getText()));
					lblPrimtreTriangle.setText(lblPrimtreTriangle.getText() + Float.toString(monTriangle.perimetre()));
					lblSurfaceTriangle.setText(lblSurfaceTriangle.getText() + Float.toString(monTriangle.surface()));

					liste_forme.add(monTriangle);
				}
				else if (textSaisieCtTriangle.getText().length() == 0 && textSaisieHtTriangle.getText().length() != 0)
				{
					lblErrSaisie.setText("Erreur : Veuillez saisir un décimal pour le côté du triangle");
					lblErrSaisie.setVisible(true);
				}
				else if (textSaisieCtTriangle.getText().length() != 0 && textSaisieHtTriangle.getText().length() == 0)
				{
					lblErrSaisie.setText("Erreur : Veuillez saisir un décimal pour la hauteur du triangle");
					lblErrSaisie.setVisible(true);
				}

				textCalculs.setText("");
				if (liste_forme.size() >= 1) {
					for (Forme Elements : liste_forme) {
						textCalculs.setText(textCalculs.getText()+Elements.affichage_calcul());
					}
				}
				}catch(Exception ee) {
					lblErrSaisie.setVisible(true);
				}

			}
		});
		btnCalc.setBounds(321, 378, 117, 25);
		contentPane.add(btnCalc);

		textSaisieCtTriangle = new JFormattedTextField();
		textSaisieCtTriangle.setBounds(288, 224, 114, 19);
		contentPane.add(textSaisieCtTriangle);
		textSaisieCtTriangle.setColumns(10);

		textSaisieHtTriangle = new JFormattedTextField();
		textSaisieHtTriangle.setBounds(288, 251, 114, 19);
		contentPane.add(textSaisieHtTriangle);
		textSaisieHtTriangle.setColumns(10);
		
		JLabel lblimgTriangle = new JLabel("");
		lblimgTriangle.setIcon(new ImageIcon("/home/etudiant/c.azibeiro/SEQ1/src/Version3/triangle.png"));
		lblimgTriangle.setBounds(32, 222, 100, 100);
		contentPane.add(lblimgTriangle);
		

	}
}
