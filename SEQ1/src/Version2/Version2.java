package Version2;

import java.util.Scanner;

/**
 * Main. 
 * @author Clement AZIBIERO
 * @version 2
 *
 */
public class Version2 {

	public static void main(String[] args) {

		Scanner saisie = new Scanner(System.in);
		int choix = 0;
		boolean ok = false;

		do {
			ok = false;
			while (!ok) {

				try {
					Methodes.AffichageMenu();
					choix = saisie.nextInt();
					Methodes.choix(choix);
					ok = true;
				} catch (Exception e) {
					System.err.println("\nErreur de saisie... Veuillez saisir un chiffre du menu");
					String vide = saisie.next();
				}
			}
		} while (choix != 9);

	}
}
