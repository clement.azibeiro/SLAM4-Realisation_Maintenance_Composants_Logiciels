package classes;
/**
 * Classe 'Licencie'
 * @author Clément AZIBEIRO
 * @version 0
 */
public class Licencie {
	private String code;
	private String nom;
	private String prenom;
	private String dateNaiss;
	private Club leClub;
	
	/**
	 * Accesseur de l'attribut 'code'
	 * @return String code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Modifieur de l'attribut 'code'
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * Accesseur de l'attribut 'nom'
	 * @return String nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * Modifieur de l'attribut 'nom'
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * Accesseur de l'attribut 'prenom'
	 * @return String
	 */
	public String getPrenom() {
		return prenom;
	}
	
	/**
	 * Modifieur de l'attribut 'prenom'
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * Accesseur de l'atribut 'DateNaiss'
	 * @return String
	 */
	public String getDateNaiss() {
		return dateNaiss;
	}
	
	/**
	 * Modifieur de l'attribut 'DateNaiss'
	 * @param dateNaiss
	 */
	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}
	
	/**
	 * Accesseur de l'attribut 'leClub'
	 * @return L'instance de la classe 'Club'
	 */
	public Club getLeClub() {
		return leClub;
	}

	/**
	 * Modifieur de l'attribut 'leClub'
	 * @param leClub : instance de la classe 'Club'
	 */
	public void setLeClub(Club leClub) {
		this.leClub = leClub;
	}

	/**
	 * Constructeur par defaut
	 */
	public Licencie() {
		this.code = "Code Licencie";
		this.nom = "Nom";
		this.prenom = "Prenom";
		this.dateNaiss = "01/01/1999";
		this.leClub = new Club();
	}
	
	/**
	 * Constructeur
	 * @param code
	 * @param nom
	 * @param prenom
	 * @param dateNaiss
	 * @param leClub
	 */
	public Licencie(String code, String nom, String prenom, String dateNaiss, Club leClub) {
		super();
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
		this.leClub = leClub;
	}

	@Override
	/**
	 * Réecriture de la méthode 'toString()'
	 * @return  Une chaîne de caractères avec la valeur de touts les attributs
	 */
	public String toString() {
		return "Licencie [code=" + code + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss
				+ ", leClub=" + leClub + "]";
	}
}
