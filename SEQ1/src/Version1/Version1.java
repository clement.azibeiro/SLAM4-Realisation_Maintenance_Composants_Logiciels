package Version1;

import java.util.Scanner;

public class Version1 {

	public static void main(String[] args) {
		Scanner saisie = new Scanner(System.in);
		int choix = 0;
		boolean ok = false;

		do {
			ok = false;
			while (!ok) {

				try {
					methodes.AffichageMenu();
					choix = saisie.nextInt();
					ok = true;
				} catch (Exception e) {
					System.err.println("\nErreur de saisie... Veuillez saisir un chiffre du menu");
					String vide = saisie.next();
				}
			}

			methodes.choix(choix);

		} while (choix != 9);
	}
}