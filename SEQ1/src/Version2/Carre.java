package Version2;

import java.util.Scanner;

/**
 * Classe 'carre' fille de 'forme'. 
 * @author Clement AZIBEIRO
 * @version 2
 */
public class Carre extends Forme {
	/**
	 * Constructeur par defaut. 
	 */
	public Carre() {
		super();
	}

	/**
	 * Constructeur. 
	 * @param cote Permet de valoriser le cote
	 */
	public Carre(float cote) {
		super(cote);
	}

	/**
	 * Mehode qui permet d'afficher la demande de saisie du cote. 
	 */
	public static void affichage_saisie_carre() {
		System.out.print("\n--> CARRE - Saisir la mesure du côté : ");
	}

	@Override
	/**
	 * Reecriture de la methode 'affichage_calcul". 
	 * @return L'affichage du calcul du carre  
	 */
	public String affichage_calcul() {
		return ("CARRE :" + super.affichage_calcul());

	}

	@Override
	/**
	 * Reecriture de la methode 'perimetre'. 
	 * @return Le perimetre du carre
	 */
	public float perimetre() {
		return getCote() * 4;
	}

	@Override
	/**
	 * Reecriture de la methode 'surface'. 
	 * @return La surface du carre
	 */
	public float surface() {
		return (float) Math.pow((double) getCote(), 2);
	}

}
