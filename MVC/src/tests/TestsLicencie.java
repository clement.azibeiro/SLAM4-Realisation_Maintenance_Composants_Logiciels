package tests;
import classes.Licencie;
import classes.Club;

public class TestsLicencie {

	public static void main(String[] args) {
		Club sonClub = new Club("RM","Real Madrid","je sais plus","Zidane");
		
		Licencie unLicencie = new Licencie();
		Licencie deuxLicencie = new Licencie("LeLicencie","AZIBEIRO","Clement","13/07/1999",sonClub);
		
		System.out.println(unLicencie.getCode()+ " "+unLicencie.getNom()+" "+unLicencie.getPrenom()+" "+unLicencie.getDateNaiss()+" "+unLicencie.getLeClub().getNom());
		System.out.println(deuxLicencie.getCode()+ " "+deuxLicencie.getNom()+" "+deuxLicencie.getPrenom()+" "+deuxLicencie.getDateNaiss()+" "+deuxLicencie.getLeClub().getNom());
		
		unLicencie.setCode("CL");
		unLicencie.setNom("Name bis");
		unLicencie.setPrenom("The Prenom");
		unLicencie.setDateNaiss("Je sais plus");
		unLicencie.setLeClub(sonClub);
		
		deuxLicencie.setCode("CA");
		deuxLicencie.setNom("AZIB");
		deuxLicencie.setPrenom("CLEM");
		deuxLicencie.setDateNaiss("Environ la");
		Club monClub = new Club();
		deuxLicencie.setLeClub(monClub);
		
		System.out.println(unLicencie.toString());
		System.out.println(deuxLicencie.toString());
	}

}
