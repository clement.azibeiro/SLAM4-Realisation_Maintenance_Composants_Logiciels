package Version2;

import java.util.Scanner;

/**
 * Classe 'forme' abstraite.    
 * 
 * @author Clement AZIBEIRO
 * @version 2
 */
public abstract class Forme {
	//Attributs
	protected float mesure;
	protected Scanner saisie = new Scanner(System.in);

	/**
	 * Accesseur de mesure. 
	 * 
	 * @return La valeur de mesure
	 */
	public float getCote() {
		return this.mesure;
	}

	/**
	 * Modifieur de mesure. 
	 * 
	 * @param s permet de modifier mesure
	 */
	public void setCote(float s) {
		this.mesure = s;
	}

	/**
	 * Constructeur par defaut. 
	 * 
	 */
	public Forme() {
		super();
		this.mesure = 0;
	}

	/**
	 * Constructeur. 
	 * 
	 * @param cote permet de valoriser mesure
	 */
	public Forme(float cote) {
		super();
		this.mesure = cote;
	}

	/**
	 * Methode abstraite pour calculer un perimetre. 
	 * 
	 * @return Un nombre floatant
	 */
	abstract public float perimetre();

	/**
	 * Methode abstraite pour calculer une surface. 
	 * 
	 * @return Un nombre floatant
	 */
	abstract public float surface();

	/**
	 * Methode void qui permet de recuperer la saisie d'un cote. 
	 */
	public void saisie_cote() {
		boolean ok = false;
		while (!ok) {
			try {
				setCote(saisie.nextFloat());
				ok = true;
			} catch (Exception e) {
				System.err.println("Erreur de type, saisir un nombre : ");
				String vide = saisie.next();
			}
		}
	}

	/**
	 * Methode String permetant d'afficher le resultat d'un calcul. 
	 * 
	 * @return Une chaine de caractere
	 */
	public String affichage_calcul() {
		return (" côté = " + getCote() + " - périmètre = " + perimetre() + " - surface = " + surface() + "\n\n");

	}
}
