package classes;

/**
 * Classe 'Club'    
 * 
 * @author Clement AZIBEIRO
 * @version 0
 */
public class Club {
	 
	private String code;
	private String nom;
	private String nom_president;
	private String nom_entraineur;
	
	/**
	 * Accesseur de l'attribut 'code'.
	 * @return String
	 */
	public String getCode() {
		return code;
	}
	/**
	 * Modifieur de l'attribut 'code'.
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * Accesseur de l'attribut 'nom'.
	 * @return String
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Modifieur de l'attribut 'nom'.
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Accesseur de l'attribut 'nom_president'.
	 * @return String
	 */
	public String getNom_president() {
		return nom_president;
	}
	/**
	 * Modifieur de l'attribut 'nom_president'.
	 * @param nom_president
	 */
	public void setNom_president(String nom_president) {
		this.nom_president = nom_president;
	}
	/**
	 * Accesseur de l'attribut 'nom_entraineur'.
	 * @return String
	 */
	public String getNom_entraineur() {
		return nom_entraineur;
	}
	/**
	 * Modifieur de l'attribut 'nom_entraineur'
	 * @param nom_entraineur
	 */
	public void setNom_entraineur(String nom_entraineur) {
		this.nom_entraineur = nom_entraineur;
	}
	
	/**
	 * Constructeur par defaut
	 */
	public Club() {
		this.code = "Code";
		this.nom = "Nom";
		this.nom_president = "Nom president";
		this.nom_entraineur = "Nom entraineur";
	}
	
	/**
	 * Constructeur
	 * @param code
	 * @param nom
	 * @param nom_president
	 * @param nom_entraineur
	 */
	public Club(String code, String nom, String nom_president, String nom_entraineur) {
		this.code = code;
		this.nom = nom;
		this.nom_president = nom_president;
		this.nom_entraineur = nom_entraineur;
	}
	
	@Override
	/**
	 * Récriture de la méthode 'toString()'
	 * @return Une chaîne de caractères avec la valeur de touts les attributs
	 */
	public String toString() {
		return "Club [code=" + code + ", nom=" + nom + ", nom_president=" + nom_president + ", nom_entraineur="
				+ nom_entraineur + "]";
	}
	
	
}
