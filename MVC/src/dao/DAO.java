package dao;

import java.sql.Connection;

import classes.Club;

/**
 * Classe abstraite 'DAO' utilisant la généricité
 * @author Clement AZIBEIRO
 * @version 0
 * @param <T> Objet instance de la classe T
 */
public abstract class DAO<T> {
	
	//Attribut(s)
	/**
	 * Attribut public 'conn' permetant d'établir la connexion a la BDD
	 */
	public Connection conn = ConnexionPostgreSql.getInstance();
	
	//Méthodes
	/**
	 * Methode abstraite permetant de créer des données
	 * @param obj Objet instance de la classe T
	 */
	public abstract void create(T obj);
	
	/**
	 * Methode abstraite permetant de lire des données
	 * @param obj Objet instance de la classe T
	 * @param cp String : Cle primaire
	 * @return 
	 */
	public abstract T read(String code);
	
	/**
	 * Methode abstraite permetant de mettre a jour des données
	 * @param obj Objet instance de la classe T
	 * @return 
	 */
	public abstract void update(T obj);
	
	/**
	 * Methode abstraite permetant de supprimer des données
	 * @param obj Objet instance de la classe T
	 */
	public abstract void delete(T obj);

}
