package Version2;

import java.util.Scanner;

/**
 * Classe 'cercle' fille de 'forme'. 
 * @author Clement AZIBEIRO
 * @version 2
 */
public class Cercle extends Forme {
	/**
	 * Constructeur par defaut. 
	 */
	public Cercle() {
		super();
	}

	/**
	 * Constructeur. 
	 * @param rayon Permet de valoriser le rayon
	 */
	public Cercle(float rayon) {
		super(rayon);
	}

	/**
	 * Mehode qui permet d'afficher la demande de saisie du rayon. 
	 */
	public static void affichage_saisie_cercle() {
		System.out.print("\n\n--> CERCLE - Saisir la mesure du rayon : ");
	}

	@Override
	/**
	 * Reecriture de la methode 'affichage_calcul". 
	 * @return L'affichage du calcul du cercle  
	 */
	public String affichage_calcul() {
		return ("CERCLE :" + super.affichage_calcul());

	}

	@Override
	/**
	 * Reecriture de la methode 'perimetre'. 
	 * @return Le perimetre du cercle
	 */
	public float perimetre() {
		return (float) (2 * Math.PI * getCote());
	}

	@Override
	/**
	 * Reecriture de la methode 'surface'. 
	 * @return La surface du cercle
	 */
	public float surface() {
		return (float) ((float) Math.pow(getCote(), 2) * Math.PI);
	}

}
