package Version2;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe 'methodes' qui permet de factoriser quelques methodes. 
 * 
 * @author Clement AZIBEIRO
 *@version 2
 */
public class Methodes {

	/**
	 * Liste de classe qui permet de stocker des instances de forme. 
	 */
	public static List<Forme> liste_forme = new ArrayList<>();

	/**
	 * Methodes void de classe qui permet d'affichier le Menu de selection. 
	 */
	public static void AffichageMenu() {
		System.out.println("\n\t>>>> Calculs Formes Géométriques <<<<\n\n");
		System.out.println("-1- CARRE\n-2- CERCLE\n-3-TRIANGLE EQUI\n\n-8- Affichage des calculs\n");
		System.out.println("-9- Quitter\n");
		System.out.print("\tVotre choix ==> ");
	}

	/**
	 * Methode void de classe permetant de faire un traitement selon le choix de l'utilisateur. 
	 * @param choix Attend un entier pour faire le traitement selon le choix de l'utilisateur.
	 */
	public static void choix(int choix) {
		if (choix == 1) {
			Carre monCarre = new Carre();
			Carre.affichage_saisie_carre();
			monCarre.saisie_cote();
			System.out.print(monCarre.affichage_calcul());
			liste_forme.add(monCarre);
		} else if (choix == 2) {
			Cercle monCercle = new Cercle();
			Cercle.affichage_saisie_cercle();
			monCercle.saisie_cote();
			System.out.print(monCercle.affichage_calcul());
			liste_forme.add(monCercle);
		} else if (choix == 3) {
			Triangle_equi monTriangle = new Triangle_equi();
			Triangle_equi.affichage_saisie_cote_triangle();
			monTriangle.saisie_cote();
			Triangle_equi.affichage_saisie_hauteur_triangle();
			monTriangle.saisie_hauteur_triangle();
			System.out.print(monTriangle.affichage_calcul());
			Methodes.liste_forme.add(monTriangle);
		} else if (choix == 8) {
			if (liste_forme.size() >= 1) {
				for (Forme Elements : liste_forme) {
					System.out.print(Elements.affichage_calcul());
				}
			} else {
				System.err.println("\nVous n'avez pas fait de calcul");
			}
		} else if (choix == 9) {
			System.out.println("\nVous avez quitter le programme...");
		} else if (choix != 9 || choix != 3 || choix != 2 || choix != 1 || choix != 8) {
			System.err.println("\nErreur de saisir... Veuillez saisie un chiffre du menu");
		}
	}

}
