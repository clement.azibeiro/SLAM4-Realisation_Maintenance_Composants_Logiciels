package Version3;

import java.util.Scanner;

/**
 * Classe 'triangle_equi' fille de 'forme'. 
 * @author Clement AZIBEIRO
 * @version 2
 */
public class Triangle_equi extends Forme {

	// Propriétés
	private float hauteur;

	/**
	 * Accesseurs de hauteur. 
	 * @return La valeur de hauteur
	 */
	public float getHauteur() {
		return this.hauteur;
	}

	/**
	 * Modifieur de hauteur
	 * @param hauteur Permet de modifier la valeur de hauteur. 
	 */
	public void setHauteur(float hauteur) {
		this.hauteur = hauteur;
	}

	/**
	 * Constructeur par defaut. 
	 */
	public Triangle_equi() {
		super();
		this.hauteur = 0;
	}

	/**
	 * Constructeur. 
	 * @param cote Permet de valoriser cote
	 * @param hauteur Permet de valoriser hauteur
	 */
	public Triangle_equi(float cote, float hauteur) {
		super(cote);
		this.mesure = hauteur;
	}

	/**
	 * Mehode qui permet d'afficher la demande de saisie du cote. 
	 */
	public static void affichage_saisie_cote_triangle() {
		System.out.print("\n\n--> TRIANGLE EQUI - Saisir la mesure du côté : ");
	}

	/**
	 * Mehode qui permet d'afficher la demande de saisie de la hauteur. 
	 */
	public static void affichage_saisie_hauteur_triangle() {
		System.out.print("\tsaisir la mesure de la hauteur : ");
	}

	/**
	 * Methode qui permet de récuperer la saisie de la hauteur du triangle
	 */
	public void saisie_hauteur_triangle() {
		boolean ok = false;
		while (!ok) {
			try {
				setHauteur(saisie.nextFloat());
				ok = true;
			} catch (Exception e) {
				System.err.println("Erreur de type, saisir un nombre : ");
				String vide = saisie.nextLine();
			}
		}
	}

	@Override
	/**
	 * Reecriture de la methode 'affichage_calcul". 
	 * @return L'affichage du calcul du triangle  
	 */
	public String affichage_calcul() {
		return ("TRIANGLE EQUI : hauteur = " + getHauteur() + super.affichage_calcul());

	}

	@Override
	/**
	 * Reecriture de la methode 'perimetre'. 
	 * @return Le perimetre du triangle
	 */
	public float perimetre() {
		return getCote() * 3;
	}

	@Override
	/**
	 * Reecriture de la methode 'surface'. 
	 * @return La surface du triangle
	 */
	public float surface() {
		return getCote() * getHauteur() / 2;
	}

}
