package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionPostgreSql {
	
	static private String url = "jdbc:postgresql://postgresql.bts-malraux72.net:5432/c.azibeiro";
	static private String user = "c.azibeiro";
	static private String passwd = "";
	static private Connection connect;
	
	public static Connection getInstance() {
		if(connect == null) {
			try {
			connect = DriverManager.getConnection(url, user, passwd);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return connect;
	}
}
