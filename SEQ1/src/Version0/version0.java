package Version0;
import java.util.Scanner;

public class version0 {

	public static void main(String[] args) 
	{
		//Déclaration des varialbles
		float cote_carre = 0, perimetre_carre = 0, surface_carre = 0, rayon_cercle = 0, 
			  perimetre_cercle = 0, surface_cercle = 0, cote_triangle_equi = 0,
		      hauteur_triangle_equi = 0, perimetre_triangle_equi = 0, surface_triangle_equi = 0;
		
		//Création d'un objet de type scanner pour enregistrer des entrées
		Scanner saisie = new Scanner(System.in);
		Scanner saisie_hauteur_triangle = new Scanner(System.in);
		
		//Affichage de la présentation du programme
		System.out.println("\t>>>> Calculs Formes Géométriques <<<<");
		
		//Saisie de l'utilisateur du coté du carré et calculs
		System.out.print("\n--> CARRE - Saisir la mesure du côté : ");
		cote_carre = saisie.nextFloat();
		perimetre_carre = cote_carre * 4;
		surface_carre = (float) Math.pow((double) cote_carre, 2);
		
		//Affichage des calculs réalisés
		System.out.print("\nCARRE : côté = "+cote_carre+
						" - périmètre = "+perimetre_carre+
						" - surface = "+surface_carre);
		
		//Saisie de l'utilisateur du rayon du cercle et calculs
		System.out.print("\n\n--> CERCLE - Saisir la mesure du rayon : ");
		rayon_cercle = saisie.nextFloat();
		perimetre_cercle = (float) (2 * Math.PI * rayon_cercle);
		surface_cercle = (float) ((float) Math.pow(rayon_cercle, 2) * Math.PI);
		
		//Affichage des calculs réalisés
		System.out.print("\nCERCLE : côté = "+rayon_cercle+
						" - périmètre = "+perimetre_cercle+
						" - surface = "+surface_cercle);
	
		//Saisie de l'utilisateur du coté puis de la hauteur et calculs
		System.out.print("\n\n--> TRIANGLE EQUI - Saisir la mesure du côté : ");
		cote_triangle_equi = saisie.nextFloat();
		System.out.print("\tsaisir la mesure de la hauteur : ");
		hauteur_triangle_equi = saisie_hauteur_triangle.nextFloat();
		perimetre_triangle_equi = cote_triangle_equi * 3;
		surface_triangle_equi = cote_triangle_equi * hauteur_triangle_equi / 2;
		
		//Affichage des calculs réalisés
		System.out.print("\nTRIANGLE EQUI : côté = "+cote_triangle_equi+
						" - hauteur = "+hauteur_triangle_equi+
						" - périmètre = "+perimetre_triangle_equi+
						" - surface = "+surface_triangle_equi);
		
	}

}
